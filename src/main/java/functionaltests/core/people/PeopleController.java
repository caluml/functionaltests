package functionaltests.core.people;

import functionaltests.core.blocklist.BlocklistResponse;
import functionaltests.error.BadGatewayException;
import functionaltests.error.BadRequestException;
import functionaltests.error.NotFoundException;
import functionaltests.error.UnauthorisedException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static java.util.stream.Collectors.toList;

@RestController
@RequestMapping("/v1/people")
public class PeopleController {

  /*
   * I've deliberately written this badly. I've stuffed as much functionality into this one class as I could.
   * You'll probably want to refactor it, extract stuff out, add a couple of Service classes, move everything out that
   * shouldn't be in a Controller.
   * Because this whole project is tested functionally, no tests should break when you do this (unless of course, you
   * break some actual functionality).
   */

  private final PersonEntityConverter personEntityConverter;
  private final RestTemplate restTemplate;
  private final String baseurl;
  private final PeopleRepository peopleRepository;

  public PeopleController(PersonEntityConverter personEntityConverter,
                          RestTemplate restTemplate,
                          @Value("${service.blocklist.baseurl}") String baseurl,
                          PeopleRepository peopleRepository) {
    this.personEntityConverter = personEntityConverter;
    this.restTemplate = restTemplate;
    this.baseurl = baseurl;
    this.peopleRepository = peopleRepository;
  }

  @GetMapping
  public GetPeopleResponse getPeople() {
    final List<Person> people = peopleRepository.findAll().stream()
      .map(personEntityConverter::convert)
      .collect(toList());

    return new GetPeopleResponse(people);
  }

  @PostMapping
  public void createPerson(@RequestBody CreatePersonRequest request) {
    if (request.getId() == null) throw new BadRequestException("id");
    if (request.getName() == null) throw new BadRequestException("name");

    try {
      final BlocklistResponse blocklistResponse = restTemplate.getForObject(baseurl + "/blocklist/" + request.getId(), BlocklistResponse.class);
      if (blocklistResponse.isOnBlocklist()) {
        throw new UnauthorisedException(request.getId() + " is on blocklist");
      }
    } catch (ResourceAccessException | HttpClientErrorException e) {
      throw new BadGatewayException("blocklist service error: " + e.getLocalizedMessage());
    }


    final PersonEntity personEntity = new PersonEntity();
    personEntity.setId(request.getId());
    personEntity.setName(request.getName());
    peopleRepository.save(personEntity);
  }

  @DeleteMapping
  public ResponseEntity delete() {
    throw new BadRequestException("Bad request");
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deletePerson(@PathVariable("id") String id) {
    try {
      peopleRepository.deleteById(id);
    } catch (EmptyResultDataAccessException e) {
      throw new NotFoundException(id);
    }
    return ResponseEntity.status(201).build();
  }


  @PutMapping
  public ResponseEntity update() {
    throw new BadRequestException("Bad request - id required");
  }

  @PutMapping("/{id}")
  public ResponseEntity update(@PathVariable("id") String id,
                               @RequestBody UpdatePersonRequest request) {
    if (request.getName() == null) throw new BadRequestException("name");

    final PersonEntity personEntity = peopleRepository.findById(id)
      .orElseThrow(() -> new NotFoundException(id));
    personEntity.setName(request.getName());
    peopleRepository.save(personEntity);
    return ResponseEntity.status(201).build();
  }

}

class GetPeopleResponse {

  private List<Person> people;

  private GetPeopleResponse() {

  }

  public GetPeopleResponse(List<Person> people) {
    this.people = people;
  }

  public List<Person> getPeople() {
    return people;
  }

  public void setPeople(List<Person> people) {
    this.people = people;
  }
}

class CreatePersonRequest {

  private String id;
  private String name;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}

class UpdatePersonRequest {

  private String name;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}

class Person {

  private String id;
  private String name;

  private Person() {
  }

  public Person(String id) {
    this.id = id;
  }

  public Person(String id,
                String name) {

    this.id = id;
    this.name = name;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
