package functionaltests.core.people;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PersonEntityConverter implements Converter<PersonEntity, Person> {

  @Override
  public Person convert(PersonEntity personEntity) {
    return new Person(personEntity.getId(), personEntity.getName());
  }
}
