package functionaltests.core.blocklist;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BlocklistResponse {

  @JsonProperty("on-blocklist")
  private boolean onBlocklist;

  public boolean isOnBlocklist() {
    return onBlocklist;
  }

  public void setOnBlocklist(boolean onBlocklist) {
    this.onBlocklist = onBlocklist;
  }
}
