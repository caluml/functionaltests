package functionaltests.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Configuration
public class ComponentConfig {

  @Bean
  RestTemplate restTemplate(@Value("${resttemplate.timeout.connect}") int connectTimeout,
                            @Value("${resttemplate.timeout.read}") int readTimeout) {
    return new RestTemplateBuilder()
      .setConnectTimeout(Duration.ofSeconds(connectTimeout))
      .setReadTimeout(Duration.ofSeconds(readTimeout))
      .build();
  }

}
