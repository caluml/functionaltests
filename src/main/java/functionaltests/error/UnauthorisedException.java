package functionaltests.error;

public class UnauthorisedException extends RuntimeException {

  private final String message;

  public UnauthorisedException(String message) {
    this.message = message;
  }

  @Override
  public String getMessage() {
    return message;
  }
}
