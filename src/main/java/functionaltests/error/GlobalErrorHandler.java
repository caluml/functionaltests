package functionaltests.error;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalErrorHandler {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  @ExceptionHandler(BadRequestException.class)
  ResponseEntity badRequest(BadRequestException e) {
    logger.error("Bad request", e);
    return getErrorResponseEntity(HttpStatus.BAD_REQUEST, e.getMessage());
  }

  @ExceptionHandler(UnauthorisedException.class)
  ResponseEntity unauthorised(UnauthorisedException e) {
    logger.error("Unauthorised", e);
    return getErrorResponseEntity(HttpStatus.FORBIDDEN, e.getMessage());
  }

  @ExceptionHandler(NotFoundException.class)
  ResponseEntity notFound(NotFoundException e) {
    logger.error("Not found", e);
    return getErrorResponseEntity(HttpStatus.NOT_FOUND, e.getMessage());
  }

  @ExceptionHandler(BadGatewayException.class)
  ResponseEntity badGateway(BadGatewayException e) {
    return getErrorResponseEntity(HttpStatus.BAD_GATEWAY, e.getMessage());
  }

  private ResponseEntity<ErrorResponse> getErrorResponseEntity(HttpStatus httpStatus,
                                                               String message) {
    return ResponseEntity
      .status(httpStatus.value())
      .contentType(MediaType.APPLICATION_JSON)
      .body(new ErrorResponse(message));
  }

  // Catch all
  @ExceptionHandler(Exception.class)
  public ResponseEntity<ErrorResponse> exception(Exception e) {
    logger.error("Uncaught exception", e);
    return ResponseEntity.status(500)
      .body(new ErrorResponse(e.getLocalizedMessage()));
  }
}
