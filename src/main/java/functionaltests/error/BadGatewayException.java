package functionaltests.error;

public class BadGatewayException extends RuntimeException {

  private final String message;

  public BadGatewayException(String message) {
    this.message = message;
  }

  @Override
  public String getMessage() {
    return message;
  }
}
