package functionaltests

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.core.annotation.AliasFor
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

/**
 * Annotation ....
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@ContextConfiguration
@ActiveProfiles
@SpringBootTest(classes = [Application.class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@interface FunctionalTest {

  @AliasFor(annotation = ContextConfiguration.class, attribute = "initializers")
  Class<? extends ApplicationContextInitializer<? extends ConfigurableApplicationContext>>[] initializers() default [FunctionalTestSpecification.class];

  @AliasFor(annotation = ActiveProfiles.class, attribute = "value")
  String[] profiles() default ["ft"];

}
