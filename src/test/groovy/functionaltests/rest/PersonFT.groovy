package functionaltests.rest

import functionaltests.FunctionalTest
import functionaltests.FunctionalTestSpecification
import io.restassured.response.Response

import static com.github.tomakehurst.wiremock.client.WireMock.*
import static io.restassured.RestAssured.given
import static io.restassured.RestAssured.when
import static org.hamcrest.Matchers.*

@FunctionalTest
class PersonFT extends FunctionalTestSpecification {


  def "Can get people"() {
    given:
      sql.executeInsert("INSERT INTO people (id, name) VALUES ('joe', 'Joe Bloggs')")
      sql.executeInsert("INSERT INTO people (id, name) VALUES ('fred', 'Fred Smith')")

    when:
      Response response = when().get(basePath() + "/v1/people")

    then:
      response.then()
        .statusCode(200)
        .contentType("application/json")
        .body("people", hasSize(2))
        .body("people[0].id", is("joe"))
        .body("people[0].name", is("Joe Bloggs"))
        .body("people[1].id", is("fred"))
        .body("people[1].name", is("Fred Smith"))
  }

  def "Can create non blocklisted people"() {
    given:
      sql.executeInsert("INSERT INTO people (id, name) VALUES ('joe', 'Joe Bloggs')")

      blocklistService.stubFor(get("/blocklist/fred")
        .willReturn(okJson("{ \"on-blocklist\": false }")))

    when:
      Response response = given()
        .contentType("application/json")
        .body("{ \"id\": \"fred\", \"name\": \"Fred Smith\" }")
        .post(basePath() + "/v1/people")

    then:
      response.then()
        .statusCode(200)

    and:
      def rows = sql.rows("SELECT * FROM people")
      rows.size == (2)
      rows.get(0).get("id") == "joe"
      rows.get(1).get("id") == "fred"
      rows.get(1).get("name") == "Fred Smith"
  }

  def "Cannot create blocklisted people"() {
    given:
      blocklistService.stubFor(get("/blocklist/fred")
        .willReturn(okJson("{ \"on-blocklist\": true }")))

      sql.executeInsert("INSERT INTO people (id, name) VALUES ('joe', 'Joe Bloggs')")

    when:
      Response response = given()
        .body("{ \"id\": \"fred\", \"name\": \"Fred Smith\" }")
        .contentType("application/json")
        .post(basePath() + "/v1/people")

    then:
      response.then()
        .statusCode(403)

    and:
      def rows = sql.rows("SELECT * FROM people")
      rows.size() == 1
  }

  def "Blocklist_service_is_erroring"() {
    given:
      blocklistService.stubFor(get("/blocklist/fred")
        .willReturn(notFound()))

    when:
      Response response = given()
        .body("{ \"id\": \"fred\", \"name\": \"Fred Smith\" }")
        .contentType("application/json")
        .post(basePath() + "/v1/people")

    then:
      response.then()
        .statusCode(502)
  }

  def "Should_timeout_blocklist_service_after_1000_ms"() {
    given:
      blocklistService.stubFor(get("/blocklist/fred")
        .willReturn(okJson("{ \"on-blocklist\": false }")
          .withFixedDelay(1010)))

    when:
      Response response = given()
        .body("{ \"id\": \"fred\", \"name\": \"Fred Smith\" }")
        .contentType("application/json")
        .post(basePath() + "/v1/people")

    then:
      response.then()
        .statusCode(502)
        .contentType("application/json")
        .body("message", startsWith("blocklist service error: I/O error on GET request for"))
  }

  def "Create_person_bad_body"() {
    when:
      Response response = given()
        .body("{ \"missing\": \"\", \"unknown\": \"Fred Smith\" }")
        .contentType("application/json")
        .post(basePath() + "/v1/people")

    then:
      response.then()
        .statusCode(400)
  }

  def "Can_delete_people"() {
    given:
      sql.executeInsert("INSERT INTO people (id, name) VALUES ('joe', 'Joe Bloggs')")
      sql.executeInsert("INSERT INTO people (id, name) VALUES ('fred', 'Fred Smith')")

    when:
      Response response = when()
        .delete(basePath() + "/v1/people/joe")

    then:
      response.then()
        .statusCode(201)

    and:
      def rows = sql.rows("SELECT * FROM people")
      rows.size == 1
      rows.get(0).get("id") == "fred"
  }

  def "Deleting_person_who_doesnt_exist"() {
    when:
      Response response = when()
        .delete(basePath() + "/v1/people/non-existent")

    then:
      response.then()
        .statusCode(404)
  }

  def "No_person_specified_to_delete"() {
    when:
      Response response = when()
        .delete(basePath() + "/v1/people")

    then:
      response.then()
        .statusCode(400)
  }

  def "Can_update_non_blocklisted_people"() {
    given:
      sql.executeInsert("INSERT INTO people (id, name) VALUES ('joe', 'Joe Bloggs')")

    when:
      Response response = given()
        .body("{\"name\": \"New Name\"}")
        .contentType("application/json")
        .put(basePath() + "/v1/people/joe")

    then:
      response.then()
        .statusCode(201)

    and:
      def rows = sql.rows("SELECT * FROM people")
      rows.size() == 1
      rows.get(0).get("id") == "joe"
      rows.get(0).get("name") == "New Name"
  }

  def "Updating_person_who_doesnt_exist"() {
    when:
      Response response = given()
        .body("{\"name\": \"New Name\"}")
        .contentType("application/json")
        .put(basePath() + "/people/nonexistent")

    then:
      response.then()
        .statusCode(404)
  }

  def "Update_person_with_bad_request"() {
    when:
      Response response = given()
        .body("{\"badrequest\": 12342}")
        .contentType("application/json")
        .put(basePath() + "/v1/people/joe")

    then:
      response.then()
        .statusCode(400)
  }

  def "Unspecified_person_to_update"() {
    when:
      Response response = when()
        .put(basePath() + "/v1/people")

    then:
      response.then()
        .statusCode(400)
  }
}
