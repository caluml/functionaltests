package functionaltests.rest

import functionaltests.FunctionalTest
import functionaltests.FunctionalTestSpecification
import org.springframework.boot.web.server.LocalServerPort

@FunctionalTest
class StartupFT extends FunctionalTestSpecification {

  @LocalServerPort
  int port


  def "HTTP port is set"() {
    expect:
      port > 0
  }

}