package functionaltests

import com.github.tomakehurst.wiremock.WireMockServer
import groovy.sql.Sql
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.testcontainers.containers.PostgreSQLContainer
import spock.lang.Shared
import spock.lang.Specification

import java.sql.SQLException

class FunctionalTestSpecification extends Specification implements ApplicationContextInitializer<ConfigurableApplicationContext> {

  // The version used in Debian 9
  public static final String TARGET_PG_VERSION = "postgres:9.6.17"

  @LocalServerPort
  private int port

  @Shared
  static WireMockServer blocklistService

  @Shared
  final static PostgreSQLContainer container = new PostgreSQLContainer<>(TARGET_PG_VERSION)
    .withUsername("test")
    .withPassword("test")

  protected static Sql sql

  // Spock method that runs before each Spec
  void setupSpec() throws Exception {
    container.start()
    while (sql == null) {
      try {
        sql = Sql.newInstance(container.getJdbcUrl(), container.getUsername(), container.getPassword(), container.getDriverClassName())
        sql.executeUpdate("CREATE TABLE people (id varchar PRIMARY KEY, name varchar NOT NULL)")
      } catch (SQLException e) {
        System.err.println(e.getMessage())
        zzzz()
      }
    }

    blocklistService = new WireMockServer()
    blocklistService.start()
  }

  // Spock method that runs after each Spec
  void cleanupSpec() {
    blocklistService.stop()
  }


  // Spock method that runs after each test
  void cleanup() throws SQLException {
    for (String table : ["people"]) {
      sql.executeUpdate("DELETE FROM " + table)
    }
    blocklistService.resetAll()
  }

  protected String basePath() {
    return "http://127.0.0.1:" + port
  }

  private static void zzzz() {
    try {
      Thread.sleep(1000)
    } catch (InterruptedException e) {
      throw new RuntimeException(e)
    }
  }

  int getServerPort() {
    return port
  }

  @Override
  void initialize(ConfigurableApplicationContext applicationContext) {
    TestPropertyValues.of(
      "spring.datasource.url=${container.getJdbcUrl()}",
      "spring.datasource.username=${container.getUsername()}",
      "spring.datasource.password=${container.getPassword()}",
      "service.blocklist.baseurl=${blocklistService.baseUrl()}",
      "resttemplate.timeout.connect=1",
      "resttemplate.timeout.read=1"
    ).applyTo(applicationContext)
  }
}
