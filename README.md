# functionaltests

Simple example Spring Boot project to demonstrate how functional tests work

# Principle

The tests should not be coupled with, or have any knowledge at all of the class, structure or workings of the service. The service should not be modified or use different classes when running for testing (other than some config properties)
When the tests are run, the following should happen
- Start a Postgres Docker container with testcontainers
- Create the required tables
- Start the service
- Execute the tests against the public interfaces of the service

# Prerequisites

- Docker

# Groovy

The tests and classes to make the tests work are written in Groovy.
You can of course do the same thing with Java.

# Classes of interest

- src/test/groovy/functionaltests/FunctionalTest.groovy
- src/test/groovy/functionaltests/rest/PersonFT.groovy
- src/main/java/functionaltests/core/people/PeopleController.java

# Tests

Run with `./test.sh`
